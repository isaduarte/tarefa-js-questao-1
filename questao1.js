function multiplicar(a, b) {
    var a_linhas = a.length,
        a_colunas = a[0].length,
        b_linhas = b.length,
        b_colunas = b[0].length,
        m = new Array(a_linhas);

    if (a_colunas != b_linhas) {
        console.log(('Matrizes incompatíveis para multiplicação'));
        return 0;
    }

    for (var linha = 0; linha < a_linhas; ++linha) {
        m[linha] = new Array(b_colunas);

        for (var coluna = 0; coluna < b_colunas; ++coluna) {
            m[linha][coluna] = 0;

            for (var i = 0; i < a_colunas; ++i) {
                m[linha][coluna] += a[linha][i] * b[i][coluna];
            }
        }
    }

    console.log('matriz')

    for (let i = 0; i < m.length; i++) {
        console.log(m[i]);
    }
}

var a1 = [[[2], [-1]], [[2], [0]]],
    b1 = [[[2], [3]], [[-2], [1]]],
    a2 = [[[4], [0]], [[-1], [-1]]],
    b2 = [[[-1], [3]], [[2], [7]]];

multiplicar(a1, b1);
multiplicar(a2, b2);